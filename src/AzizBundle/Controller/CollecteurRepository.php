<?php
/**
 * Created by PhpStorm.
 * User: cor
 * Date: 15/04/2019
 * Time: 20:31
 */
namespace Pidev\AzizBundle\Controller;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

class CollecteurRepository extends  EntityRepository
{
    public function findByrecherchecollecteur($codepostal) {
        $query=$this->getEntityManager()->createQuery("Select c from PidevAzizBundle:Collecteur c WHERE m.codepostal=:codepostal")
        ->setParameter('codepostal',$codepostal);
        return $query->getResult();
    }
}