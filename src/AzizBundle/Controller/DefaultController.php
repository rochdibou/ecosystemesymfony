<?php

namespace Pidev\AzizBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('@PidevAziz/Aziz/welcome.html.twig');
    }
    public function index2Action()
    {
        return $this->render('@PidevAziz/Aziz/base.html.twig');
    }
    public function index3Action()
    {
        return $this->render('@PidevAziz/Default/index.html.twig');
    }
}

