<?php

namespace Pidev\AzizBundle\Controller;

use Pidev\AzizBundle\Entity\Collecteur;
use Pidev\AzizBundle\Entity\Appareil;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Collecteur controller.
 *
 * @Route("collecteur")
 */
class CollecteurController extends Controller
{
    /**
     * Lists all collecteur entities.
     *
     * @Route("/collecteurindex{id}", name="collecteur_index")
     * @Method("GET")
     */
    public function indexcollecteurAction(Appareil $appareil)
    {
        $em = $this->getDoctrine()->getManager();

        $collecteurs = $em->getRepository('PidevAzizBundle:Collecteur')->findBy(array('codepostal' =>$appareil->getCodepostal()));


        return $this->render('@PidevAziz/Appareil/Affichecollecteur.html.twig', array(
            'collecteurs' => $collecteurs,
        ));
    }

    /**
     * Creates a new collecteur entity.
     *
     * @Route("/new", name="collecteur_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $collecteur = new Collecteur();
        $form = $this->createForm('Pidev\AzizBundle\Form\CollecteurType', $collecteur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($collecteur);
            $em->flush();

            return $this->redirectToRoute('collecteur_show', array('id' => $collecteur->getId()));
        }

        return $this->render('collecteur/new.html.twig', array(
            'collecteur' => $collecteur,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a collecteur entity.
     *
     * @Route("/{id}", name="collecteur_show")
     * @Method("GET")
     */
    public function showAction(Collecteur $collecteur)
    {
        $deleteForm = $this->createDeleteForm($collecteur);

        return $this->render('collecteur/show.html.twig', array(
            'collecteur' => $collecteur,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing collecteur entity.
     *
     * @Route("/{id}/edit", name="collecteur_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Collecteur $collecteur)
    {
        $deleteForm = $this->createDeleteForm($collecteur);
        $editForm = $this->createForm('Pidev\AzizBundle\Form\CollecteurType', $collecteur);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('collecteur_edit', array('id' => $collecteur->getId()));
        }

        return $this->render('collecteur/edit.html.twig', array(
            'collecteur' => $collecteur,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a collecteur entity.
     *
     * @Route("/{id}", name="collecteur_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Collecteur $collecteur)
    {
        $form = $this->createDeleteForm($collecteur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($collecteur);
            $em->flush();
        }

        return $this->redirectToRoute('collecteur_index');
    }

    /**
     * Creates a form to delete a collecteur entity.
     *
     * @param Collecteur $collecteur The collecteur entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Collecteur $collecteur)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('collecteur_delete', array('id' => $collecteur->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
