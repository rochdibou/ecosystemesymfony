<?php

namespace Pidev\AzizBundle\Controller;

use Pidev\AzizBundle\Entity\Appareil;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Appareil Controller.
 * @Route("appareil")
*/
class AppareilController extends Controller
{
    /**
     * Lists all appareil entities.
     *
     * @Route("/indexappareil", name="appareil_index")
     * @Method("GET")
     */
    public function afficheAppareilAction()
    {
        $em = $this->getDoctrine()->getManager();

        $appareils = $em->getRepository('PidevAzizBundle:Appareil')->findAll();

        return $this->render('@PidevAziz/Appareil/index.html.twig', array(
            'appareils' => $appareils,
        ));
    }

    /**
     * Creates a new appareil entity.
     *
     * @Route("/addappareil", name="appareil_new")
     * @Method({"GET", "POST"})
     */
    public function AddAppareilAction(Request $request)
    {
        $appareil = new Appareil();
        $form = $this->createForm('Pidev\AzizBundle\Form\AppareilType', $appareil);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($appareil);
            $em->flush();
            return $this->redirectToRoute('appareil_index', array('id' => $appareil->getId()));
        }



        return $this->render('@PidevAziz/Appareil/Appareil.html.twig', array(
            'appareil' => $appareil,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a appareil entity.
     *
     * @Route("/showappareil{id}", name="appareil_show")
     * @Method("GET")
     */
    public function showAppareilAction(Appareil $appareil)
    {
        $deleteForm = $this->createDeleteForm($appareil);

        return $this->render('@PidevAziz/Appareil/Collecteur.html.twig', array(
            'appareil' => $appareil,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing appareil entity.
     *
     * @Route("/{id}/editappareil", name="appareil_edit")
     * @Method({"GET", "POST"})
     */
    public function editAppareilAction(Request $request, Appareil $appareil)
    {
        $deleteForm = $this->createDeleteForm($appareil);
        $editForm = $this->createForm('Pidev\AzizBundle\Form\AppareilType', $appareil);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('appareil_show', array('id' => $appareil->getId()));
        }

        return $this->render('@PidevAziz/Appareil/edit.html.twig', array(
            'appareil' => $appareil,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a appareil entity.
     *
     * @Route("/{id}/deleteappareil", name="appareil_delete")
     * @Method("DELETE")
     */
    public function deleteAppareilAction(Request $request, Appareil $appareil)
    {
        $form = $this->createDeleteForm($appareil);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($appareil);
            $em->flush();

        }
        return $this->redirectToRoute('appareil_index');


    }

    /**
     * Creates a form to delete a appareil entity.
     *
     * @param Appareil $appareil The appareil entity
     *

     */
    private function createDeleteForm(Appareil $appareil)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('appareil_delete', array('id' => $appareil->getId())))
            ->setMethod('DELETE')
            ->getForm();

    }


}
