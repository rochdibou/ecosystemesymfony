<?php

namespace Pidev\AzizBundle\Controller;

use Pidev\AzizBundle\Entity\Posts;
use Pidev\AzizBundle\Form\PostsType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class PostsController extends Controller
{
    public function addpostAction(Request $request)
    {

        $post = new Posts();
        $form =$this->createForm (PostsType::class, $post);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {

            $em=$this->getDoctrine()->getManager();
            $file=$post->getImagePost();
            $filename= md5(uniqid())  .  '.'  . $file->guessExtension();
            $file->move($this->getParameter('photos_directory'));
            $post->setImagePost($filename);
            $post->setHeurePost(new \DateTime('now'));
            
            $em->persist($post);
            $em->flush();

        }

        return $this->render('@PidevAziz/Aziz/addpost.html.twig',array('f'=>$form->createView()));
    }
    public function listpostAction(Request $request)
    {

        $em=$this->getDoctrine()->getManager();
        $posts=$em->getRepository(Posts::class)->findAll();
        return $this->render('@PidevAziz/Aziz/listpost.html.twig', array(
            "posts" =>$posts
        ));

    }

}

