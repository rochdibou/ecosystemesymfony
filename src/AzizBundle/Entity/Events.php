<?php

namespace Pidev\AzizBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Events
 *
 * @ORM\Table(name="events", indexes={@ORM\Index(name="idU", columns={"id_user"})})
 * @ORM\Entity
 */
class Events
{
    /**
     * @var integer
     *
     * @ORM\Column(name="Event_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $eventId;

    /**
     * @var string
     *
     * @ORM\Column(name="Event_Nom", type="string", length=30, nullable=false)
     */
    private $eventNom;

    /**
     * @var string
     *
     * @ORM\Column(name="Event_Description", type="string", length=255, nullable=false)
     */
    private $eventDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="Event_Lieu", type="string", length=30, nullable=false)
     */
    private $eventLieu;

    /**
     * @var string
     *
     * @ORM\Column(name="Event_Date", type="string", length=30, nullable=false)
     */
    private $eventDate;

    /**
     * @var string
     *
     * @ORM\Column(name="Event_Heure", type="string", length=30, nullable=false)
     */
    private $eventHeure;

    /**
     * @var string
     *
     * @ORM\Column(name="Photo", type="string", length=100, nullable=true)
     */
    private $photo;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_user", referencedColumnName="id")
     * })
     */
    private $idUser;



    /**
     * Set eventNom
     *
     * @param string $eventNom
     *
     * @return Events
     */
    public function setEventNom($eventNom)
    {
        $this->eventNom = $eventNom;

        return $this;
    }

    /**
     * Get eventNom
     *
     * @return string
     */
    public function getEventNom()
    {
        return $this->eventNom;
    }

    /**
     * Set eventDescription
     *
     * @param string $eventDescription
     *
     * @return Events
     */
    public function setEventDescription($eventDescription)
    {
        $this->eventDescription = $eventDescription;

        return $this;
    }

    /**
     * Get eventDescription
     *
     * @return string
     */
    public function getEventDescription()
    {
        return $this->eventDescription;
    }

    /**
     * Set eventLieu
     *
     * @param string $eventLieu
     *
     * @return Events
     */
    public function setEventLieu($eventLieu)
    {
        $this->eventLieu = $eventLieu;

        return $this;
    }

    /**
     * Get eventLieu
     *
     * @return string
     */
    public function getEventLieu()
    {
        return $this->eventLieu;
    }

    /**
     * Set eventDate
     *
     * @param string $eventDate
     *
     * @return Events
     */
    public function setEventDate($eventDate)
    {
        $this->eventDate = $eventDate;

        return $this;
    }

    /**
     * Get eventDate
     *
     * @return string
     */
    public function getEventDate()
    {
        return $this->eventDate;
    }

    /**
     * Set eventHeure
     *
     * @param string $eventHeure
     *
     * @return Events
     */
    public function setEventHeure($eventHeure)
    {
        $this->eventHeure = $eventHeure;

        return $this;
    }

    /**
     * Get eventHeure
     *
     * @return string
     */
    public function getEventHeure()
    {
        return $this->eventHeure;
    }

    /**
     * Set photo
     *
     * @param string $photo
     *
     * @return Events
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Get eventId
     *
     * @return integer
     */
    public function getEventId()
    {
        return $this->eventId;
    }

    /**
     * Set idUser
     *
     * @param \Pidev\AzizBundle\Entity\Utilisateur $idUser
     *
     * @return Events
     */
    public function setIdUser(\Pidev\AzizBundle\Entity\Utilisateur $idUser = null)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return \Pidev\AzizBundle\Entity\Utilisateur
     */
    public function getIdUser()
    {
        return $this->idUser;
    }
}
