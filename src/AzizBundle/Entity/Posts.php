<?php

namespace Pidev\AzizBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Posts
 *
 * @ORM\Table(name="posts", indexes={@ORM\Index(name="idU", columns={"id_user"})})
 * @ORM\Entity
 */
class Posts
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_post", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idPost;

    /**
     * @var string
     *
     * @ORM\Column(name="titre_post", type="string", length=255, nullable=false)
     */
    private $titrePost;

    /**
     * @var string
     *
     * @ORM\Column(name="description_post", type="string", length=255, nullable=false)
     */
    private $descriptionPost;

    /**
     * @var string
     *
     * @ORM\Column(name="image_post", type="string", length=255, nullable=false)
     */
    private $imagePost;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="heure_post", type="datetime", nullable=false)
     */
    private $heurePost;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_user", referencedColumnName="id")
     * })
     */
    private $idUser;



    /**
     * Set titrePost
     *
     * @param string $titrePost
     *
     * @return Posts
     */
    public function setTitrePost($titrePost)
    {
        $this->titrePost = $titrePost;

        return $this;
    }

    /**
     * Get titrePost
     *
     * @return string
     */
    public function getTitrePost()
    {
        return $this->titrePost;
    }

    /**
     * Set descriptionPost
     *
     * @param string $descriptionPost
     *
     * @return Posts
     */
    public function setDescriptionPost($descriptionPost)
    {
        $this->descriptionPost = $descriptionPost;

        return $this;
    }

    /**
     * Get descriptionPost
     *
     * @return string
     */
    public function getDescriptionPost()
    {
        return $this->descriptionPost;
    }

    /**
     * Set imagePost
     *
     * @param string $imagePost

     * @return Posts
     */
    public function setImagePost($imagePost)
    {
        $this->imagePost = $imagePost;

        return $this;
    }

    /**
     * Get imagePost
     *
     * @return string
     */
    public function getImagePost()
    {
        return $this->imagePost;
    }

    /**
     * Set heurePost
     *
     * @param \DateTime $heurePost
     *
     * @return Posts
     */
    public function setHeurePost($heurePost)
    {
        $this->heurePost = $heurePost;

        return $this;
    }

    /**
     * Get heurePost
     *
     * @return \DateTime
     */
    public function getHeurePost()
    {
        return $this->heurePost;
    }

    /**
     * Get idPost
     *
     * @return integer
     */
    public function getIdPost()
    {
        return $this->idPost;
    }

    /**
     * Set idUser
     *
     * @return Posts
     */
    public function setIdUser(\Pidev\UserBundle\Entity\User $idUser )
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return \Pidev\UserBundle\Entity\User
     */
    public function getIdUser()
    {
        return $this->idUser;
    }
}
