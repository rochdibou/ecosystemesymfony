<?php

namespace Pidev\AzizBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Projet
 *
 * @ORM\Table(name="projet", indexes={@ORM\Index(name="idU", columns={"id_user"})})
 * @ORM\Entity
 */
class Projet
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_projet", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idProjet;

    /**
     * @var string
     *
     * @ORM\Column(name="titre_idee", type="string", length=11, nullable=false)
     */
    private $titreIdee;

    /**
     * @var string
     *
     * @ORM\Column(name="description_idee", type="string", length=30, nullable=false)
     */
    private $descriptionIdee;

    /**
     * @var string
     *
     * @ORM\Column(name="type_idee", type="string", length=30, nullable=false)
     */
    private $typeIdee;

    /**
     * @var string
     *
     * @ORM\Column(name="duree", type="string", length=200, nullable=false)
     */
    private $duree;

    /**
     * @var integer
     *
     * @ORM\Column(name="budget", type="integer", nullable=false)
     */
    private $budget;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_user", referencedColumnName="id")
     * })
     */
    private $idUser;



    /**
     * Set titreIdee
     *
     * @param string $titreIdee
     *
     * @return Projet
     */
    public function setTitreIdee($titreIdee)
    {
        $this->titreIdee = $titreIdee;

        return $this;
    }

    /**
     * Get titreIdee
     *
     * @return string
     */
    public function getTitreIdee()
    {
        return $this->titreIdee;
    }

    /**
     * Set descriptionIdee
     *
     * @param string $descriptionIdee
     *
     * @return Projet
     */
    public function setDescriptionIdee($descriptionIdee)
    {
        $this->descriptionIdee = $descriptionIdee;

        return $this;
    }

    /**
     * Get descriptionIdee
     *
     * @return string
     */
    public function getDescriptionIdee()
    {
        return $this->descriptionIdee;
    }

    /**
     * Set typeIdee
     *
     * @param string $typeIdee
     *
     * @return Projet
     */
    public function setTypeIdee($typeIdee)
    {
        $this->typeIdee = $typeIdee;

        return $this;
    }

    /**
     * Get typeIdee
     *
     * @return string
     */
    public function getTypeIdee()
    {
        return $this->typeIdee;
    }

    /**
     * Set duree
     *
     * @param string $duree
     *
     * @return Projet
     */
    public function setDuree($duree)
    {
        $this->duree = $duree;

        return $this;
    }

    /**
     * Get duree
     *
     * @return string
     */
    public function getDuree()
    {
        return $this->duree;
    }

    /**
     * Set budget
     *
     * @param integer $budget
     *
     * @return Projet
     */
    public function setBudget($budget)
    {
        $this->budget = $budget;

        return $this;
    }

    /**
     * Get budget
     *
     * @return integer
     */
    public function getBudget()
    {
        return $this->budget;
    }

    /**
     * Get idProjet
     *
     * @return integer
     */
    public function getIdProjet()
    {
        return $this->idProjet;
    }

    /**
     * Set idUser
     *
     * @param \Pidev\AzizBundle\Entity\Utilisateur $idUser
     *
     * @return Projet
     */
    public function setIdUser(\Pidev\AzizBundle\Entity\Utilisateur $idUser = null)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return \Pidev\AzizBundle\Entity\Utilisateur
     */
    public function getIdUser()
    {
        return $this->idUser;
    }
}
