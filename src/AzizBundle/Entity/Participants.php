<?php

namespace Pidev\AzizBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Participants
 *
 * @ORM\Table(name="participants", indexes={@ORM\Index(name="fk_event_tbparticipant", columns={"id_event"}), @ORM\Index(name="idU", columns={"id_user"})})
 * @ORM\Entity
 */
class Participants
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_participant", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idParticipant;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_user", referencedColumnName="id")
     * })
     */
    private $idUser;

    /**
     * @var \Events
     *
     * @ORM\ManyToOne(targetEntity="Events")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_event", referencedColumnName="Event_id")
     * })
     */
    private $idEvent;



    /**
     * Get idParticipant
     *
     * @return integer
     */
    public function getIdParticipant()
    {
        return $this->idParticipant;
    }

    /**
     * Set idEvent
     *
     * @param \Pidev\AzizBundle\Entity\Events $idEvent
     *
     * @return Participants
     */
    public function setIdEvent(\Pidev\AzizBundle\Entity\Events $idEvent = null)
    {
        $this->idEvent = $idEvent;

        return $this;
    }

    /**
     * Get idEvent
     *
     * @return \Pidev\AzizBundle\Entity\Events
     */
    public function getIdEvent()
    {
        return $this->idEvent;
    }

    /**
     * Set idUser
     *
     * @param \Pidev\AzizBundle\Entity\Utilisateur $idUser
     *
     * @return Participants
     */
    public function setIdUser(\Pidev\AzizBundle\Entity\Utilisateur $idUser = null)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return \Pidev\AzizBundle\Entity\Utilisateur
     */
    public function getIdUser()
    {
        return $this->idUser;
    }
}
