<?php

namespace Pidev\AzizBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Comments
 *
 * @ORM\Table(name="comments", indexes={@ORM\Index(name="idU", columns={"id_user"}), @ORM\Index(name="fk_idpost", columns={"id_post"})})
 * @ORM\Entity
 */
class Comments
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_comment", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idComment;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_post", type="integer", nullable=false)
     */
    private $idPost;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=false)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="heure_comment", type="datetime", nullable=false)
     */
    private $heureComment;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_user", referencedColumnName="id")
     * })
     */
    private $idUser;



    /**
     * Set idPost
     *
     * @param integer $idPost
     *
     * @return Comments
     */
    public function setIdPost($idPost)
    {
        $this->idPost = $idPost;

        return $this;
    }

    /**
     * Get idPost
     *
     * @return integer
     */
    public function getIdPost()
    {
        return $this->idPost;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Comments
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set heureComment
     *
     * @param \DateTime $heureComment
     *
     * @return Comments
     */
    public function setHeureComment($heureComment)
    {
        $this->heureComment = $heureComment;

        return $this;
    }

    /**
     * Get heureComment
     *
     * @return \DateTime
     */
    public function getHeureComment()
    {
        return $this->heureComment;
    }

    /**
     * Get idComment
     *
     * @return integer
     */
    public function getIdComment()
    {
        return $this->idComment;
    }

    /**
     * Set idUser
     *
     * @param \Pidev\AzizBundle\Entity\Utilisateur $idUser
     *
     * @return Comments
     */
    public function setIdUser(\Pidev\AzizBundle\Entity\Utilisateur $idUser = null)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return \Pidev\AzizBundle\Entity\Utilisateur
     */
    public function getIdUser()
    {
        return $this->idUser;
    }
}
