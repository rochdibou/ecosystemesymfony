<?php

namespace Pidev\AzizBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Idee
 *
 * @ORM\Table(name="idee", indexes={@ORM\Index(name="idU", columns={"id_user"})})
 * @ORM\Entity
 */
class Idee
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre_idee", type="string", length=30, nullable=false)
     */
    private $titreIdee;

    /**
     * @var string
     *
     * @ORM\Column(name="description_idee", type="string", length=255, nullable=false)
     */
    private $descriptionIdee;

    /**
     * @var string
     *
     * @ORM\Column(name="type_idee", type="string", length=30, nullable=false)
     */
    private $typeIdee;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_user", referencedColumnName="id")
     * })
     */
    private $idUser;



    /**
     * Set titreIdee
     *
     * @param string $titreIdee
     *
     * @return Idee
     */
    public function setTitreIdee($titreIdee)
    {
        $this->titreIdee = $titreIdee;

        return $this;
    }

    /**
     * Get titreIdee
     *
     * @return string
     */
    public function getTitreIdee()
    {
        return $this->titreIdee;
    }

    /**
     * Set descriptionIdee
     *
     * @param string $descriptionIdee
     *
     * @return Idee
     */
    public function setDescriptionIdee($descriptionIdee)
    {
        $this->descriptionIdee = $descriptionIdee;

        return $this;
    }

    /**
     * Get descriptionIdee
     *
     * @return string
     */
    public function getDescriptionIdee()
    {
        return $this->descriptionIdee;
    }

    /**
     * Set typeIdee
     *
     * @param string $typeIdee
     *
     * @return Idee
     */
    public function setTypeIdee($typeIdee)
    {
        $this->typeIdee = $typeIdee;

        return $this;
    }

    /**
     * Get typeIdee
     *
     * @return string
     */
    public function getTypeIdee()
    {
        return $this->typeIdee;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idUser
     *
     * @param \Pidev\AzizBundle\Entity\Utilisateur $idUser
     *
     * @return Idee
     */
    public function setIdUser(\Pidev\AzizBundle\Entity\Utilisateur $idUser = null)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return \Pidev\AzizBundle\Entity\Utilisateur
     */
    public function getIdUser()
    {
        return $this->idUser;
    }
}
