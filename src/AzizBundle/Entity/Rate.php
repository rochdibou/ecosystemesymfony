<?php

namespace Pidev\AzizBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rate
 *
 * @ORM\Table(name="rate")
 * @ORM\Entity
 */
class Rate
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_rate", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idRate;

    /**
     * @var integer
     *
     * @ORM\Column(name="idpr", type="integer", nullable=false)
     */
    private $idpr;

    /**
     * @var integer
     *
     * @ORM\Column(name="rating", type="integer", nullable=false)
     */
    private $rating;

    /**
     * @var integer
     *
     * @ORM\Column(name="iduser", type="integer", nullable=false)
     */
    private $iduser;



    /**
     * Set idpr
     *
     * @param integer $idpr
     *
     * @return Rate
     */
    public function setIdpr($idpr)
    {
        $this->idpr = $idpr;

        return $this;
    }

    /**
     * Get idpr
     *
     * @return integer
     */
    public function getIdpr()
    {
        return $this->idpr;
    }

    /**
     * Set rating
     *
     * @param integer $rating
     *
     * @return Rate
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get rating
     *
     * @return integer
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Set iduser
     *
     * @param integer $iduser
     *
     * @return Rate
     */
    public function setIduser($iduser)
    {
        $this->iduser = $iduser;

        return $this;
    }

    /**
     * Get iduser
     *
     * @return integer
     */
    public function getIduser()
    {
        return $this->iduser;
    }

    /**
     * Get idRate
     *
     * @return integer
     */
    public function getIdRate()
    {
        return $this->idRate;
    }
}
