<?php

namespace ecosysteme\adminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('ecosystemeuserBundle::accueil.html.twig');
    }
}
