<?php

namespace ecosysteme\adminBundle\Controller;

use ecosysteme\userBundle\Entity\Membre;
use Ob\HighchartsBundle\Highcharts\Highchart;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Endroid\OpenWeatherMap\Client;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class backController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('', array('name' => $name));
    }
    public function backAction(){
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('ecosystemeuserBundle:Membre')->findAll();
        $tabmembre = array();
        $tabadmin = array();
        $tabsuper = array();
        foreach ($user as $value) {
            foreach ($value->getRoles() as $x) {
                if ($x == 'ROLE_ADMIN') {
                    array_push($tabadmin, $value);

                }
                if ($x == 'ROLE_MEMBRE') {
                    array_push($tabmembre, $value);
                }
                if ($x == 'ROLE_SUPER_ADMIN'){
                    array_push($tabsuper, $value);
                }
            }


        }
        $nbradmin = count($tabadmin);
        $nbrmembre = count($tabmembre);
        $nbrsuper =count($tabsuper);

        $ob1 = new Highchart();
        $ob1->chart->renderTo('piechart');
        $ob1->title->text('poucentage utilisateurs de site');
        $ob1->plotOptions->pie(array(
            'allowPointSelect' => true,
            'cursor' => 'pointer',
            'dataLabels' => array('enabled' => false),
            'showInLegend' => true
        ));
        $data = array(
            array('les Admins', $nbradmin * 100 / count($user)),
            array('les Membres', $nbrmembre * 100 / count($user)),
            array('les super admin', $nbrsuper * 100/count($user)),

        );
        $ob1->series(array(array('type' => 'pie', 'name' => 'Browser share', 'data' => $data)));
        return $this->render('ecosystemeadminBundle::backAdmin.html.twig'
        ,array('ob'=>$ob1,'ob1'=>'ob2','ob2'=>'ob3'));
    }

    public function utilAction(){
        $em = $this->getDoctrine()->getManager();
        $utils = $em->getRepository('ecosystemeuserBundle:Membre')->findAll();
        $ok= array('ROLE_MEMBRE','ROLE_USER');
        return $this->render('ecosystemeadminBundle::utlAdmin.html.twig', array(
            'utils' => $utils,'ok'=>$ok
        ));

    }
    public function admAction(){
        $em = $this->getDoctrine()->getManager();
        $utils = $em->getRepository('ecosystemeuserBundle:Membre')->findAll();
        $ok= array('ROLE_ADMIN','ROLE_USER');
        return $this->render('ecosystemeadminBundle::admAdmin.html.twig', array(
            'utils' => $utils,'ok'=>$ok
        ));

    }
    public function supAction(Request $request,$id){


            $em = $this->getDoctrine()->getManager();
        $ok=$em->getRepository('ecosystemeuserBundle:Membre')->find($id);
            $em->remove($ok);
            $em->flush();

        return $this->redirectToRoute('ecosystemeadmin_util');
    }
    public function sup1Action(Request $request,$id){


        $em = $this->getDoctrine()->getManager();
        $ok=$em->getRepository('ecosystemeuserBundle:Membre')->find($id);
        $em->remove($ok);
        $em->flush();

        return $this->redirectToRoute('ecosystemeadmin_adm');
    }
    public function admAddAction(Request $request){
        $adm=new Membre();
        if ($request->isMethod('post')){
            $em=$this->getDoctrine()->getManager();
            $adm->setUsername($request->get('login'));
            $adm->setPassword($request->get('pwd'));
            $adm->setEmail($request->get('mail'));
            $adm->setRoles(array('ROLE_ADMIN'));
            $adm->setEnabled(1);
            $adm->setPassword('$2y$13$0ytMj7ie8iJWLIXPfenKRO7gE298riO2oqAClwRIGJnnVre9mfwra');
            $adm->setPhotoMembre('photo_membre/1.jpg');
            $em->persist($adm);
            $em->flush();
            return $this->redirectToRoute('ecosystemeadmin_adm');

        }


        return $this->render('ecosystemeadminBundle::addAdmin.html.twig');
    }
 
    public function chtAction()
    {
        $client = $this->get('endroid.openweathermap.client');
        $weather = $client->getWeather('Breda,nl');
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('ecosystemeuserBundle:Membre')->findAll();
        $tabmembre = array();
        $tabadmin = array();
        $tabsuper = array();
        foreach ($user as $value) {
            foreach ($value->getRoles() as $x) {
                if ($x == 'ROLE_ADMIN') {
                    array_push($tabadmin, $value);

                }
                if ($x == 'ROLE_MEMBRE') {
                    array_push($tabmembre, $value);
                }
                if ($x == 'ROLE_SUPER_ADMIN'){
                    array_push($tabsuper, $value);
                }
            }
        }
        $nbradmin = count($tabadmin);
        $nbrmembre = count($tabmembre);
        $nbrsuper =count($tabsuper);
        $ob = new Highchart();
        $ob->chart->renderTo('piechart');
        $ob->title->text('poucentage utilisateurs de site');
        $ob->plotOptions->pie(array(
            'allowPointSelect' => true,
            'cursor' => 'pointer',
            'dataLabels' => array('enabled' => false),
            'showInLegend' => true
        ));
        $data = array(
            array('les Admins', $nbradmin * 100 / count($user)),
            array('les Membres', $nbrmembre * 100 / count($user)),
            array('les super admin', $nbrsuper * 100/count($user)),

        );
        $ob->series(array(array('type' => 'pie', 'name' => 'Browser share', 'data' => $data)));





        return $this->render('ecosystemeadminBundle::charts1.html.twig',
            array(
                'chart' => $ob
            ));


    }


    public function showIdeeAction(){
            $em = $this->getDoctrine()->getManager();
            $idees = $em->getRepository('ecosystemeuserBundle:idee')->findAll();
            return $this->render('ecosystemeadminBundle::list.html.twig', array(
                'idees' => $idees,
            ));
    }
    public function deleteIdeeAction(Request $request, $id)
    {
            $em = $this->getDoctrine()->getManager();
            $idee = $em->getRepository('ecosystemeuserBundle:idee')->find($id);
            var_dump($idee);
            exit();
            $em->remove($idee);
            $em->flush();
        

        return $this->redirectToRoute('idee_admin_show');
    }



   
 



}
