<?php

namespace ecosysteme\userBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Idee
 *
 * @ORM\Table(name="idee", indexes={@ORM\Index(name="id_user", columns={"id_user"})})
 * @ORM\Entity
 */
class idee
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre_idee", type="string", length=30, nullable=false)
     */
    private $titreIdee;

    /**
     * @var string
     *
     * @ORM\Column(name="description_idee", type="string", length=255, nullable=false)
     */
    private $descriptionIdee;

    /**
     * @var string
     *
     * @ORM\Column(name="type_idee", type="string", length=30, nullable=false)
     */
    private $typeIdee;

    /**
     * @var \Membre
     *
     * @ORM\ManyToOne(targetEntity="membre")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_user", referencedColumnName="id")
     * })
     */
    private $idUser;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitreIdee()
    {
        return $this->titreIdee;
    }

    /**
     * @param string $titreIdee
     */
    public function setTitreIdee($titreIdee)
    {
        $this->titreIdee = $titreIdee;
    }

    /**
     * @return string
     */
    public function getDescriptionIdee()
    {
        return $this->descriptionIdee;
    }

    /**
     * @param string $descriptionIdee
     */
    public function setDescriptionIdee($descriptionIdee)
    {
        $this->descriptionIdee = $descriptionIdee;
    }

    /**
     * @return string
     */
    public function getTypeIdee()
    {
        return $this->typeIdee;
    }

    /**
     * @param string $typeIdee
     */
    public function setTypeIdee($typeIdee)
    {
        $this->typeIdee = $typeIdee;
    }


    public function __construct()
    {
        $this->idUser = new ArrayCollection();
    }

    /**
     * @return \Utilisateur
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * @param \Utilisateur $idUser
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;
    }


}
