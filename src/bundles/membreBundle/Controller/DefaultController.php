<?php

namespace ecosysteme\membreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('ecosystememembreBundle:Default:index.html.twig');
    }
}
