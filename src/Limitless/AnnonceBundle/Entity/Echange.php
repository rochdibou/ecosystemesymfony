<?php

namespace Limitless\AnnonceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Echange
 *
 * @ORM\Table(name="echange")
 * @ORM\Entity
 */
class Echange
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idechange", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idechange;

    /**
     * @var integer
     *
     * @ORM\Column(name="iduserP", type="integer", nullable=false)
     */
    private $iduserp;

    /**
     * @var integer
     *
     * @ORM\Column(name="iduserR", type="integer", nullable=false)
     */
    private $iduserr;

    /**
     * @var integer
     *
     * @ORM\Column(name="produitP", type="integer", nullable=false)
     */
    private $produitp;

    /**
     * @var integer
     *
     * @ORM\Column(name="produitR", type="integer", nullable=false)
     */
    private $produitr;

    /**
     * @var integer
     *
     * @ORM\Column(name="etat", type="integer", nullable=false)
     */
    private $etat;


}

