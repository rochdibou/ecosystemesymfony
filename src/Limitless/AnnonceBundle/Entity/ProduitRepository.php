<?php
/**
 * Created by PhpStorm.
 * User: asus
 * Date: 12/04/2019
 * Time: 00:50
 */

namespace Limitless\AnnonceBundle\Entity;


use Doctrine\ORM\EntityRepository;

class ProduitRepository extends EntityRepository
{
    public function findAllPosts()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT a
         FROM AnnonceBundle:Produit a
      
         ORDER BY a.posted_at DESC'
            )
            ->getArrayResult();
    }


}