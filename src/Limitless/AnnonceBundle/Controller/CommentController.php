<?php

namespace Limitless\AnnonceBundle\Controller;

use Limitless\AnnonceBundle\Entity\Comment;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Limitless\AnnonceBundle\Form;
use Symfony\Component\HttpFoundation\Request;

class CommentController extends Controller
{


/*{{ form_start(toShow) }}
<div class="form-group">
        {{ form_errors(toShow.content) }}
        {{ form_widget(toShow.content, { 'attr':  {
            'rows': '4',
            'class': 'form-control',
            'placeholder': 'Enter your comment'
        }}) }}
    </div> */
    public function ModifierAction(Request $request)
    {
        $em=$this->getDoctrine()->getManager();
        $comment=$em->getRepository(Comment::class)->find($request->get('id'));
        $form=$this->createForm(Form\CommentType::class,$comment);
        $form->handleRequest($request);
        if($form->isSubmitted())
        {
            $em->merge($comment);
            $em->flush();
            return $this->redirectToRoute('limitless_annonce_afficherComment');
        }
        return $this->render('@LimitlessAnnonce/Comment/modifier.html.twig',array('form'=>$form->createView()));
    }
}
