<?php

namespace Limitless\AnnonceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('@LimitlessAnnonce/Default/index.html.twig');
    }
    public function homeAction()
    {
        return $this->render('@LimitlessAnnonce/Default/home.html.twig');
    }
}
