<?php

namespace Limitless\AnnonceBundle\Controller;

use Limitless\AnnonceBundle\Entity\Comment;
use Limitless\AnnonceBundle\Entity\Produit;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Limitless\AnnonceBundle\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;


class ProduitController extends Controller
{
    public function AjouterAction(request $request)
    {
        $produit=new Produit();
        $Form=$this->createForm(Form\ProduitType::class,$produit);
        $Form->handleRequest($request);
        if($Form->isSubmitted())
        {
            $em=$this->getDoctrine()->getManager();
            $file = $produit->getImage();
            $filename = md5(uniqid()) . '.' . $file->guessExtension();
            $file->move($this->getParameter('photos_directory'), $filename);
            $produit->setImage($filename);
            $em->persist($produit);
            $em->flush();
            return $this->redirectToRoute('limitless_annonce_afficherProduit');
        }
        return $this->render('@LimitlessAnnonce/Produit/ajouter.html.twig', array('form'=>$Form->createView()));
    }
    public function AfficherAction()
    {
        $em=$this->getDoctrine()->getManager();
        $produits=$em->getRepository(Produit::class)->findAll();

        return $this->render('@LimitlessAnnonce/Produit/afficher.html.twig',array('p'=>$produits));
    }
    public function ModifierAction(Request $request,$id)
    {
        $em = $this->getDoctrine()->getManager();
        $produit = $em->getRepository(Produit::class)->find($id);
        $Form = $this->createForm(Form\ProduitType::class,$produit);
        if ($Form->handleRequest($request)->isValid()) {

            $file = $produit->getImage();
            $filename = md5(uniqid()) . '.' . $file->guessExtension();
            $file->move($this->getParameter('photos_directory'), $filename);
            $produit->setImage($filename);
            $em->persist($produit);
            $em->flush();
            return $this->redirectToRoute('limitless_annonce_afficherProduit');
        }
        return $this->render('@LimitlessAnnonce/Produit/modifier.html.twig',array('form'=>$Form->createView()));
    }

    public function SupprimerAction(Request $request)
    {
        $em=$this->getDoctrine()->getManager();
        $em->remove($em->getRepository(Produit::class)->find($request->get('id')));
        $em->flush();
        return $this->redirectToRoute('limitless_annonce_afficherProduit');
    }
    public function AjouterCommentAction(Request $request)
    {
        $comment = new Comment();
        if ($request->isMethod('POST')) {


            $id = $request->get('id');

            $comment->setProduit($id);

            //$comment->setProduit($id);

            //$em=$this->getDoctrine()->getManager();


            //GET THE ID OF THE PRODUCT :
            //$query=$em->createQuery("SELECT p.id FROM LimitlessAnnonceBundle:Produit p WHERE p.id =:id")
            // ->setParameter('id', $id)
            //->getOneOrNullResult();


            //$comment->setUser($user);

            $comment->setContent($request->request->get('comment'));
            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();

            return $this->redirectToRoute('limitless_annonce_afficherProduit');
        }

    }

    public function detailsAction(Request $request)
    {
        $comment = new Comment();
        $produit= new Produit();

        //$user= $this->getUser();

        $id = $request->get('id');
        $em=$this->getDoctrine()->getManager();

//        $query = $em->createQuery('select p.id from LimitlessAnnonceBundle:Produit p where p.id= :id_p');
//        $query->SetParameter('id_p',$id);
//        $produit_id =  $query->execute();
//
////        var_dump($produit_id[0]['id']);



        //GET ALL COMMENTS :
        $query = $em->createQuery('select c.content from LimitlessAnnonceBundle:Comment c where c.produit= :id');
        $query->SetParameter('id',$id);
        $comments =  $query->execute();

        //GET THE ID OF THE PRODUCT :
        $query=$em->createQuery("SELECT p.id FROM LimitlessAnnonceBundle:Produit p WHERE p.id =:id")
            ->setParameter('id', $id)
            ->getOneOrNullResult();

        var_dump($query['id']);

        $toShow = $em
            ->getRepository(Produit::class)
            ->find($id);

        return $this->render('@LimitlessAnnonce/Produit/afficherdetails.html.twig', array(
            "toShow" => $toShow ,'c'=>$comments));
    }
    public function rechercherAction(Request $request)
    {
        if($request->isMethod('POST'))
        {
            $em=$this->getDoctrine()->getManager();
            $m=$em->getRepository(Produit::class)->findAll(["nom"=>$request->get('nom')]);
            return $this->render('@LimitlessAnnonce/Produit/rechercher.html.twig',["p"=>$m[0]]);
        }
        return $this->render('@LimitlessAnnonce/Produit/rechercher.html.twig',["p"=>null]);
    }
   /* public function AfficherCommentAction()
    {
        $em=$this->getDoctrine()->getManager();
        $comments=$em->getRepository(Comment::class)->findAll();
        return $this->render('@LimitlessAnnonce/Produit/afficherdetails.html.twig',array('c'=>$comments));
    }*/
//    public function AjouterCommentAction(Request $request)
//    {
//        $comment = new Comment();
//        $form=$this->createForm(Form\CommentType::class,$comment);
//        $form->handleRequest($request);
//        if ($form->isSubmitted())
//        {
//            $em=$this->getDoctrine()->getManager();
//            $em->persist($comment);
//            $em->flush();
//            return $this->redirectToRoute('limitless_annonce_afficherComment');
//        }
//        return $this->render('@LimitlessAnnonce/Produit/afficherdetails.html.twig', array('form'=>$form->createView()));
//
//    }

    public function SupprimeCommentrAction(Request $request)
    {
        $em=$this->getDoctrine()->getManager();
        $em->remove($em->getRepository(Comment::class)->find($request->get('id')));
        $em->flush();
        return $this->redirectToRoute('limitless_annonce_afficherComment');
    }

}
