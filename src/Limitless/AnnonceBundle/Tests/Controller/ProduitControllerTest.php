<?php

namespace AnnonceBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProduitControllerTest extends WebTestCase
{
    public function testAjouter()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'AjouterProduit');
    }

    public function testModifier()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'ModifierProduit');
    }

    public function testSupprimer()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'SupprimerProduit');
    }

    public function testAfficher()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'AfficherProduit');
    }

}
