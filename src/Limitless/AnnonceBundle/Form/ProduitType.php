<?php

namespace Limitless\AnnonceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ProduitType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom',TextType::class,[
                'attr'=>[
                    'placeholder'=>"Nom de l'annonce"
                ]
            ])->add('categorie', ChoiceType::class, array(
                'choices'=> [
                    'choisissez votre catégorie'=>[
                        'Machine a laver' => "Machine a laver",
                        'Refrigerator' => "Refrigerator",
                        'Plaque de cuisson' => "Rlaque de cuisson",
                        'Four électrique' => "Four électrique",
                        'Micro-ondes' => "micro-ondes",
                        'Chauffage' => "Chauffage",
                        'Climatiseur' => "Climatiseur",
                        'Television' => "Television",
                        'Autres...' => "Autres...",
                    ],
                ]))->add('description' ,TextareaType::class,[
                    'attr'=>[
                        'placeholder'=>"Description de l'annonce"
                    ]
            ])->add('prix')
            ->add('genre', TextType::class,[
                'attr'=>[
                    'placeholder'=>"Lieux"
                ]
            ])->add('image',FileType::class,array('data_class'=>null));
        //->add('image');
            //->add('save', SubmitType::class, [
              // 'label'=>'Enregistre votre annonce'
            //]);
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Limitless\AnnonceBundle\Entity\Produit'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'limitless_annoncebundle_produit';
    }


}
