<?php

namespace Limitless\ProjectBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('LimitlessProjectBundle:Default:index.html.twig');
    }
}
